function Render(jsondata) {
	//模板渲染的赋值
	var data = eval(jsondata);
	var gettpl = $("#stock").html();
	laytpl(gettpl).render(data, function(html){
		console.log(html)
		$("#view").html(html);
	});		
}

var currentColumn;
$(document).ready(function () {
    // 连接服务端
	 var socket = io('http://117.30.36.193:2120');
    // 连接后登录
    socket.on('connect', function(){
    	socket.emit('login', 'stock');
    });
    // 后端推送来消息时
    socket.on('new_msg', function(msg){
		var ischeck =$("#isfolow").attr('checked')
		//console.log(ischeck);
		if(ischeck =="checked"){
			Render( eval("(" + Base64.decode(msg) + ")") );
			if(currentColumn!=null){
				clickTH(currentColumn);
			}
		}
    });
});



		var clickTH = function(t){
				var tableObject = $('#tableSort'); //获取id为tableSort的table对象  
				var tbHead = tableObject.children('thead'); //获取table对象下的thead  
				var tbHeadTh = tbHead.find('th'); //获取thead下的tr下的th  
				var thisIndex = $(t).index();
				var dataType = $(t).attr("type");//点击时获取当前th的type属性值  
				//console.log(dataType,thisIndex);
	            checkColumnValue(thisIndex, dataType);  		
				currentColumn = t;
		}
			var sortIndex = -1;  
 
		
            //对表格排序  
            function checkColumnValue(index, type) {  

				var tableObject = $('#tableSort'); //获取id为tableSort的table对象  
				var tbHead = tableObject.children('thead'); //获取table对象下的thead  
				var tbHeadTh = tbHead.find('tr th'); //获取thead下的tr下的th  
				var tbBody = tableObject.children('tbody'); //获取table对象下的tbody  
				var tbBodyTr = tbBody.find('tr'); //获取tbody下的tr  
                var trsValue = new Array();  
  
                tbBodyTr.each(function () {  
                    var tds = $(this).find('td');  
                    //获取行号为index列的某一行的单元格内容与该单元格所在行的行内容添加到数组trsValue中  
                    trsValue.push(type + ".separator" + $(tds[index]).html() + ".separator" + $(this).html());  
                    $(this).html("");  
                });  
  
                var len = trsValue.length;  
				console.log(index,sortIndex);
                if (index == sortIndex) {  
                //如果已经排序了则直接倒序  
                    trsValue.reverse();  
                } else {  
                    for (var i = 0; i < len; i++) {  
                        //split() 方法用于把一个字符串分割成字符串数组  
                        //获取每行分割后数组的第一个值,即此列的数组类型,定义了字符串\数字\Ip  
                        type = trsValue[i].split(".separator")[0];  
                        for (var j = i + 1; j < len; j++) {  
                            //获取每行分割后数组的第二个值,即文本值  
                            value1 = trsValue[i].split(".separator")[1];  
                            //获取下一行分割后数组的第二个值,即文本值  
                            value2 = trsValue[j].split(".separator")[1];  
                            //接下来是数字\字符串等的比较  
                            if (type == "number") {  
                                value1 = value1 == "" ? 0 : value1;  
                                value2 = value2 == "" ? 0 : value2;  
                                if (parseFloat(value1) > parseFloat(value2)) {  
                                    var temp = trsValue[j];  
                                    trsValue[j] = trsValue[i];  
                                    trsValue[i] = temp;  
                                }  
                            } else {  
                                if (value1.localeCompare(value2) > 0) {//该方法不兼容谷歌浏览器  
                                    var temp = trsValue[j];  
                                    trsValue[j] = trsValue[i];  
                                    trsValue[i] = temp;  
                                }  
                            }  
                        }  
                    }  
                }
  
                for (var i = 0; i < len; i++) {  
                    $("tbody tr:eq(" + i + ")").html(trsValue[i].split(".separator")[2]);  
                }  

				//sortIndex = index;
            }  